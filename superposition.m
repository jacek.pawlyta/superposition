% Matlab R2019b code to demontrate FFT ability to remove noise from a
% signal
% licence: GPL 2.0
% author: Jacek Pawlyta
% date: 21.01.2020
% changes:
% 10.10.2021: correct some typos

clear; 
m = 9; % round(m/2) = liczba dodanych sinusoid
f = 1;  % częstotliwość podstawowa w Hz  
A = 1;  % amplituda podstawowa w m 
tMax = 10; % czas maksymalny w s

sampling = 1000; % ilość próbek na sekundę
sampleSize = tMax*sampling+1; %

t  = [0:1/sampling:tMax]; % czas w s 

data = zeros(size(t));

for k=1:2:m;
    amp = A/k;
    omega = 2*pi*f*k;
    data = data + amp*sin(omega*t);
end


%lets add some noise to the periodic functions
for n = 1:size(data,2)
    data(n) =  data(n) + 2*randn(1); % comment out this line to get signal without noise
end

plot(t,data)
title("superpozycja")
xlabel("czas, s")
ylabel("amplituda, m")

pause;

analysis = fft(data);

powerSpectrum = analysis.*conj(analysis)/sampleSize;
fr = sampling*(0:sampleSize-1)/sampleSize;

plot(fr,powerSpectrum)
axis([0 3*m min(powerSpectrum) max(powerSpectrum)])
title("analiza fft")
xlabel("częstotliwość, Hz")
ylabel("PSD, m²/Hz")

